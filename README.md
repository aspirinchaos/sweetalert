# Sweet alert 2

Пакет обертка для [sweetalert](https://sweetalert2.github.io/).

Подключает стили и реализует mixin для bs4, confirm, success, error.

Экспортирует объект Popup.

### Mixins

#### swalWithBootstrapButtons

Содержит классы что бы отобразить bs4 кнопоки и перевод для кнопок

#### confirm

Показывает кнопку "Отмена" по дефолту

#### success

Дефолтная иконка "success"

#### error

Дефолтная иконка "error"

### Экспорт 
```javascript
const Popup = {
  // sweetalert без mixins
  SwalClean,
  Swal: swalWithBootstrapButtons,
  confirm,
  success,
  error,
};
// meteor не поддерживает default экспорт
export { Popup };
```

### Использование
```javascript
// import from atmosphere package
import { Popup } from 'meteor/sweetalert';
// Отображение подтверждения
Popup.confirm({
  title: 'Are you sure?',
  text: 'You will not be able to recover this imaginary file!',
  type: 'warning',
// когда нужно выполнить реакцию пользователя
}).then((result) => {
  if (result.value) {
    // title, text и type можно передать как параметры
    Popup.Swal('Deleted!', 'Your file has been deleted.', 'success');
  // когда нужно отработать определенное закрытие окна
  } else if (result.dismiss === Popup.Swal.DismissReason.cancel) {
    Popup.Swal('Cancelled', 'Your imaginary file is safe :)', 'error');
  }
});
// отображение успешного выполнения
// title, text как параметры
Popup.success('Good job!', 'You clicked the button!');
// отображение ошибки
// title, text как ключи объекта
Popup.error({ title: 'Oops...', text: 'Something went wrong!' });
```


### TODO

- Тестирование 

