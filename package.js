Package.describe({
  name: 'sweetalert',
  version: '0.1.2',
  summary: 'Wrapper for sweetalert2',
  git: 'https://bitbucket.org/aspirinchaos/sweetalert.git',
  documentation: 'README.md',
});

Npm.depends({
  sweetalert2: '7.26.9',
});

Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use(['ecmascript', 'fourseven:scss']);
  api.addFiles('src/sweetalert2.scss', 'client');
  api.mainModule('sweetalert.js');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('sweetalert');
  api.mainModule('sweetalert-tests.js');
});
