import SwalClean from 'sweetalert2/dist/sweetalert2';

/**
 * @module meteor/sweetalert
 */

const swalWithBootstrapButtons = SwalClean.mixin({
  confirmButtonText: 'Ок',
  confirmButtonClass: 'btn btn-primary',
  cancelButtonClass: 'btn btn-danger ml-3',
  cancelButtonText: 'Отмена',
  buttonsStyling: false,
});

const confirm = swalWithBootstrapButtons.mixin({
  type: 'warning',
  showCancelButton: true,
});

const confirmRemove = confirm.mixin({
  confirmButtonText: 'Удалить',
  confirmButtonClass: 'btn btn-danger',
  cancelButtonClass: 'btn btn-primary ml-3',
  cancelButtonText: 'Оставить',
});

const question = swalWithBootstrapButtons.mixin({
  type: 'question',
  confirmButtonText: 'Да',
  cancelButtonText: 'Нет',
  showCancelButton: true,
});

const success = swalWithBootstrapButtons.mixin({
  type: 'success',
});

const error = swalWithBootstrapButtons.mixin({
  type: 'error',
});

/**
 * @memberOf meteor/sweetalert
 */
const Popup = {
  SwalClean,
  Swal: swalWithBootstrapButtons,
  confirm,
  success,
  error,
  question,
  confirmRemove,
};

export { Popup };
